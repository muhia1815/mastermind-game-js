-- migrate:up
CREATE TABLE users(
    id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    username VARCHAR NOT NULL UNIQUE,
    password VARCHAR NOT NULL,
    avatar VARCHAR
);

-- migrate:down
DROP TABLE users;
