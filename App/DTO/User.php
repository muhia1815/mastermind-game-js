<?php


namespace App\DTO;


class User {
    public ?int $id;
    public string $username;
    public string $password;
    public ?string $avatar;

    public static function fromArray(array $query_result): self {
        $user = new self;
        $user->id = $query_result['id'];
        $user->username = $query_result['username'];
        $user->password = $query_result['password'];
        $user->avatar = $query_result['avatar'];
        return $user;
    }
}