<?php

namespace App;

use PDO;

class Database {
    private static ?PDO $instance = null;

    private function __construct(){
        // no-op
    }

    public static function getInstance(): PDO {
        if(self::$instance == null){
            self::_buildPDO();
        }

        return self::$instance;
    }

    private static function _buildPDO() {
        self::$instance = new PDO(
            sprintf("pgsql:host=%s;port=%d;dbname=%s", DATABASE_HOST, DATABASE_PORT, DATABASE_DATABASE),
            DATABASE_USERNAME,
            DATABASE_PASSWORD,
            [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            ]
        );
    }

}