<?php


namespace App\Validators;


class PasswordValidator {
    public static function validatePassword(?string $password){
        $errors = [];

        if(is_null($password)){
            return ['Le mot de passe est obligatoire'];
        }

        if(strlen($password) < 8){
            $errors[] = 'Le mot de passe doit faire au minimum 8 caractères';
        }

        return $errors;
    }
}