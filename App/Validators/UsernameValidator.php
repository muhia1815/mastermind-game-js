<?php


namespace App\Validators;


use App\Repository\UsersRepository;

class UsernameValidator {
    public static function validate(?string $username, UsersRepository $users_repository): array {
        $errors = [];

        if(is_null($username)){
            return ["Le nom d'utilisateur est obligatoire"];
        }

        if(strlen($username) < 4 || strlen($username) > 15){
            $errors[] = "Le nom d'utilisateur doit faire au minimum 4 caractères, au maximum 15 caractères";
        }

        if(!preg_match('/^[a-z0-9-_]+$/i', $username)){
            $errors[] = "Le nom d'utilisateur ne doit contenir que des caractères alphanumérique, des tirets et des underscores";
        }

        if($users_repository->getUserByUsername($username) != null){
            $errors[] = "Ce nom d'utilisateur est déjà pris. Veuillez en choisir un autre";
        }

        return $errors;
    }
}