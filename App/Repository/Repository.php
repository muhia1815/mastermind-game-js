<?php


namespace App\Repository;


use PDO;

abstract class Repository {
    protected PDO $db;

    public function __construct(PDO $db) {
        $this->db = $db;
    }
}