<?php


namespace App\Repository;


use App\DTO\User;

class UsersRepository extends Repository {
    public function getUserByUsername(string $username): ?User {
        $query = $this->db->prepare('SELECT * FROM users WHERE username = ?');
        $query->execute([$username]);

        return $query->rowCount() > 0 ? User::fromArray($query->fetch()) : null;
    }

    public function getUserById(int $id): ?User {
        $query = $this->db->prepare('SELECT * FROM users WHERE id = ?');
        $query->execute([$id]);

        return $query->rowCount() > 0 ? User::fromArray($query->fetch()) : null;
    }

    public function insert(User $user): void {
        $query = $this->db->prepare('INSERT INTO users(username, password) VALUES(?, ?)');
        $query->execute([$user->username, $user->password]);
        $user->id = $this->db->lastInsertId();
    }

    public function updateUser(User $user) {
        $query = $this->db->prepare('UPDATE users SET username = ?, password = ?, avatar = ? WHERE id = ?');
        $query->execute([
            $user->username,
            $user->password,
            $user->avatar,
            $user->id
        ]);
    }
}