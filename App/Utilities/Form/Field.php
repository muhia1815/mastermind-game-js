<?php


namespace App\Utilities\Form;


class Field {
    private string $label;
    private array $errors = [];
    private array $attributes = [];

    public function __construct(string $type, string $name, string $label){
        $this->label = $label;

        $this->attributes['id'] = $name;
        $this->attributes['name'] = $name;
        $this->attributes['type'] = $type;
    }

    public function setValue(?string $value): Field {
        $this->attributes['value'] = $value;
        return $this;
    }

    public function setErrors(array $errors): Field {
        $this->errors = $errors;
        return $this;
    }

    public function setHint(string $hint): Field {
        $this->attributes['hint'] = $hint;
        return $this;
    }

    public function pushAttributes(array $attributes): Field {
        $this->attributes = array_merge($this->attributes, $attributes);
        return $this;
    }

    public function __toString(): string {
        $label = $this->label;
        $errors = $this->errors;
        $attributes = $this->attributes;

        if(!empty($attributes['value'])){
            $attributes['value'] = htmlspecialchars($attributes['value']);
        }

        if(!empty($attributes['hint'])){
            $hint = htmlspecialchars($attributes['hint']);
            unset($attributes['hint']);
        } else {
            $hint = null;
        }

        ob_start();
        require ROOT . '/includes/partials/form/field.php';
        return ob_get_clean();
    }
}