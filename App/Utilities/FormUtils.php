<?php


namespace App\Utilities;


class FormUtils {
    public static function fieldError($errors){
        require ROOT . '/includes/partials/form_error.php';
    }

    // Taken from Laravel Framework: Illuminate\View\ComponentAttributeBag::__toString
    public static function arrayToAttributes(array $attributes){
        $string = '';

        foreach ($attributes as $key => $value) {
            if ($value === false || is_null($value)) {
                continue;
            }

            if ($value === true) {
                $value = $key;
            }

            $string .= ' '.$key.'="'.str_replace('"', '\\"', trim($value)).'"';
        }

        return trim($string);
    }
}