<?php


namespace App\Utilities;


use App\DTO\User;
use App\Repository\UsersRepository;
use InvalidArgumentException;

class Auth {
    const USER_KEY = 'user_id';

    private static ?User $currentUser = null;
    private static bool $hasRequestedUser = false;

    public static function isUserAuthenticated(){
        return isset($_SESSION[self::USER_KEY]);
    }

    public static function login($user){
        if($user instanceof User){
            $user_id = $user->id;
        } elseif(is_int($user)) {
            $user_id = $user;
        } else {
            throw new InvalidArgumentException("The user must either be a " . User::class . ' or an integer');
        }

        $_SESSION[self::USER_KEY] = $user_id;
    }

    public static function logout(){
        unset($_SESSION[self::USER_KEY]);
    }

    public static function getUser(UsersRepository $repository): ?User {
        if(self::$hasRequestedUser){
            return self::$currentUser;
        }

        self::$hasRequestedUser = true;

        if(!self::isUserAuthenticated()){
            return null;
        }

        $user = $repository->getUserById($_SESSION[self::USER_KEY]);

        if(is_null($user)){
            return null;
        }

        self::$currentUser = $user;
        return self::$currentUser;
    }

    public static function requireLoggedInUser(UsersRepository $repository){
        if(is_null(self::getUser($repository))){
            header('Location: /login.php');
            exit;
        }
    }
}