<?php


namespace App\Utilities;


class SessionUtils {
    private const FLASH_KEY = 'flash';

    public static function flash(string $key, $value): void {
        self::_ensureFlashExists();
        $_SESSION[self::FLASH_KEY][$key] = $value;
    }

    public static function flashExists(string $key): bool {
        return isset($_SESSION[self::FLASH_KEY][$key]);
    }

    public static function flashGet(string $key, $default = null){
        return $_SESSION[self::FLASH_KEY][$key] ?? $default;
    }

    public static function decayFlash(){
        if(!isset($_SESSION[self::FLASH_KEY])){
            return;
        }

        $_SESSION[self::FLASH_KEY]['_ttl']--;

        if($_SESSION[self::FLASH_KEY]['_ttl'] == 0){
            unset($_SESSION[self::FLASH_KEY]);
        }
    }

    private static function _ensureFlashExists(){
        if(!isset($_SESSION[self::FLASH_KEY])){
            $_SESSION[self::FLASH_KEY] = [
                '_ttl' => 2,
            ];
        }
    }
}