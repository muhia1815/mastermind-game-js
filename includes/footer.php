        </div>
    </main>

    <?php foreach($scripts ?? [] as $script): ?>
        <?php if(is_array($script)): ?>
            <script src="<?= $script['file'] ?>" type="<?= $script['type'] ?>"></script>
        <?php else: ?>
            <script src="<?= $script ?>"></script>
        <?php endif; ?>
    <?php endforeach; ?>
</body>
</html>