<?php

use App\Utilities\SessionUtils;

define('ROOT', dirname(__DIR__));

require_once "constants.php";
require_once "autoloader.php";
require_once "functions.php";

session_start();

SessionUtils::decayFlash();