<?php
function send_json(array $data){
    header('Content-Type: application/json');
    echo json_encode($data);
    exit;
}

function create_temporary_directory(string $prefix): string {
    $temporary_prefix = sys_get_temp_dir();
    $random_name = bin2hex(random_bytes(8));
    $temporary_path = "$temporary_prefix/$prefix$random_name";
    mkdir($temporary_path);

    return $temporary_path;
}