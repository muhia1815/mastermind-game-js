<?php

/**
 * @param string $class La classe à charger autoamtiquement
 *
 * Cette fonction prend un nom de classe PHP et son espace de nom à charger automatiquement,
 * ("App\Repository\UserRepository" par exemple) et transforme le tout en un chemin complet.
 *
 * Le fichier en question est ensuite inclus. Comme il contient la classe, la classe demandée
 * existe et PHP peut continuer l'exécution de la page.
 */
function autoload_class(string $class){
    $realpath = ROOT . "/" . str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';

    if(file_exists($realpath)){
        require_once $realpath;
    }
}

spl_autoload_register('autoload_class');