<?php
/**
 * @var string $label
 * @var array $errors
 * @var array $attributes
 */
?>
<div class="form-group <?= !empty($errors) ? 'has-error' : '' ?>">
    <label for="<?= $attributes['id'] ?>" class="form-label"><?= htmlspecialchars($label) ?></label>
    <input <?= \App\Utilities\FormUtils::arrayToAttributes($attributes) ?> class="form-input">
    <?php if(!empty($hint)): ?>
        <p class="form-input-hint"><?= $hint ?></p>
    <?php endif; ?>
    <?php \App\Utilities\FormUtils::fieldError($errors) ?>
</div>