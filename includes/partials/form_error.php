<?php
/**
 * @var array $errors
 */
?>

<?php if(!empty($errors)): ?>
    <?php if(count($errors) == 1): ?>
        <p class="form-input-hint"><?= htmlspecialchars($errors[0]) ?></p>
    <?php else: ?>
        <ul class="form-input-hint">
            <?php foreach($errors as $error): ?>
                <li><?= htmlspecialchars($error) ?></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
<?php endif; ?>
