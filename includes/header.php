<?php

use App\Database;
use App\Repository\UsersRepository;
use App\Utilities\Auth;
use App\Utilities\SessionUtils;
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/assets/css/spectre.min.css">

    <?php foreach($stylesheets ?? [] as $stylesheet): ?>
        <link rel="stylesheet" href="<?= $stylesheet ?>">
    <?php endforeach; ?>

    <title><?= htmlspecialchars($title ?? 'Sans nom 1') ?> - RIA Mindmaster</title>
</head>
<body>
    <header>
        <nav class="navbar">
            <section class="navbar-section">
                <a href="/" class="btn btn-link">Accueil</a>
                <a href="/game.php" class="btn btn-link">Jouer</a>
                <a href="/description/description_start.html">Description du jeu</a>
                <a href="/bricoles.php" class="btn btn-link">Autres bricoles</a>
            </section>

            <section class="navbar-section">
                <?php if(Auth::isUserAuthenticated()): $user = Auth::getUser(new UsersRepository(Database::getInstance())) ?>
                    <a href="#" class="btn btn-link" disabled="">
                        Bienvenue <img src="/user-content/<?= $user->avatar ?>" alt="Avatar" class="avatar avatar-xs">
                        <?= htmlspecialchars($user->username) ?>
                    </a>
                    <a href="/disconnect.php" class="btn btn-link">Se déconnecter</a>
                    <a href="/account/profile.php" class="btn btn-link">Voir mon compte</a>
                <?php else: ?>
                    <a href="/register.php" class="btn btn-link">S'inscrire</a>
                    <a href="/login.php" class="btn btn-link">Se connecter</a>
                <?php endif; ?>
            </section>
        </nav>
    </header>

    <main class="container">
        <div class="column col-11 col-mx-auto col-sm-12">
            <?php if(($session_error = SessionUtils::flashGet('error'))): ?>
                <p class="toast toast-error"><?= htmlspecialchars($session_error) ?></p>
            <?php endif; ?>

            <?php if(($session_success = SessionUtils::flashGet('success'))): ?>
                <p class="toast toast-success"><?= htmlspecialchars($session_success) ?></p>
            <?php endif; ?>
