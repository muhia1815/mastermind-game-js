#!/bin/bash

case $1 in
    "phase1")
        timedatectl set-timezone Europe/Zurich
        timedatectl

        apt-get update
        apt-get upgrade -y
        apt-get install nginx php7.4-{fpm,curl,gd,json,pgsql,readline,sqlite3,zip,xdebug} postgresql curl -y
        rm /etc/nginx/sites-enabled/default
        echo > /dev/null

        curl -Lo /usr/bin/dbmate https://github.com/amacneil/dbmate/releases/download/v1.12.0/dbmate-linux-amd64
        chmod +x /usr/bin/dbmate
        ;;

    "phase2")
        # Copie des fichiers dans la VM
        mv /tmp/vagrant/nginx.vhost.conf /etc/nginx/sites-enabled/ria.hevs.conf
        mv /tmp/vagrant/php.ini /etc/php/7.4/fpm/conf.d/30-vagrant.ini

        mv /tmp/vagrant/postgresql/pg_hba.conf /etc/postgresql/13/main/pg_hba.conf
        mv /tmp/vagrant/postgresql/postgresql.conf /etc/postgresql/13/main/postgresql.conf

        chown postgres:postgres /etc/postgresql/13/main/pg_hba.conf
        chown postgres:postgres /etc/postgresql/13/main/postgresql.conf

        systemctl restart nginx postgresql@13-main php7.4-fpm

        # Attendre que PostgreSQL soit en route
        while ! nc -z 127.0.0.1 5432; do
          sleep 0.1
        done

        sudo -u postgres psql < /tmp/vagrant/setupdb.sql

        ;;
    *)
        echo "Unknown phase $1"
        ;;
esac
