<?php

use App\Database;
use App\DTO\User;
use App\Repository\UsersRepository;
use App\Utilities\Form\Field;
use App\Utilities\SessionUtils;
use App\Validators\PasswordValidator;
use App\Validators\UsernameValidator;

require_once "../includes/init.php";

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['register'])) {
    // Traiter les valeurs du formulaire
    $errors = [];

    $username = $_POST['username'] ?? null;
    $password = $_POST['password'] ?? null;
    $password_confirmation = $_POST['password_confirmation'] ?? null;

    $db = Database::getInstance();
    $users_repository = new UsersRepository($db);

    $e = UsernameValidator::validate($username, $users_repository);
    if (!empty($e)) {
        $errors['username'] = $e;
    }

    $e = PasswordValidator::validatePassword($password);
    if (!empty($e)) {
        $errors['password'] = $e;
    } elseif ($password != $password_confirmation) {
        $errors['password_confirmation'][] = "Les mots de passe ne sont pas identiques";
        $errors['password'][] = "Les mots de passe ne sont pas identiques";
    }

    if (empty($errors)) {
        $password = password_hash($password, PASSWORD_DEFAULT);
        $user = User::fromArray(['id' => null, 'username' => $username, 'password' => $password, 'avatar' => null]);
        $users_repository->insert($user);

        SessionUtils::flash('success', 'Votre inscription a bien été prise en compte');
        $_SESSION['user_id'] = $user->id;

        header('Location: /');
    } else {
        SessionUtils::flash('error', 'Une erreur s\'est produite lors de l\'enregistrement. Merci de vérifier les champs');
        SessionUtils::flash('errors', $errors);
        SessionUtils::flash('old_post', $_POST);

        // Rediriger vers la même page pour faire une requête GET au lieu d'arriver sur une page où on a fait POST.
        // Cela permet à l'utilisateur de rafraîchir la page sans devoir se payer un message du navigateur web utilisé.
        header('Location: /register.php');
    }

    exit;
}

$old_values = SessionUtils::flashGet('old_post', []);
$errors = SessionUtils::flashGet('errors', []);

$title = "S'inscrire";
require_once ROOT . "/includes/header.php";
?>

    <div class="column col-sm-12 col-md-8 col-4 col-mx-auto">
        <form action="/register.php" method="post">
            <?=
            (new Field('text', 'username', "Nom d'utilisateur"))
                ->setErrors($errors['username'] ?? [])
                ->setValue($old_values['username'] ?? null)
            ?>

            <?=
            (new Field('password', 'password', "Mot de passe"))
                ->setErrors($errors['password'] ?? [])
                ->setValue($old_values['password'] ?? null)
            ?>

            <?=
            (new Field('password', 'password_confirmation', "Confirmation du mot de passe"))
                ->setErrors($errors['password_confirmation'] ?? [])
                ->setValue($old_values['password_confirmation'] ?? null)
            ?>

            <input type="submit" name="register" value="S'inscrire" class="btn btn-primary">
        </form>
    </div>

<?php
require_once ROOT . "/includes/footer.php";
