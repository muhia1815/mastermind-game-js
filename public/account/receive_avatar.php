<?php

use App\Database;
use App\Repository\UsersRepository;
use App\Utilities\Auth;

require_once '../../includes/init.php';

const MAXIMUM_FILE_SIZE = 5_000_000; // 5 MB
const MAXIMUM_IMAGE_WIDTH = 72;
const MAXIMUM_IMAGE_HEIGHT = 72;
const ALLOWED_EXTENSIONS = ['png', 'jpg', 'jpeg'];

if(!Auth::isUserAuthenticated()){
    header('HTTP/1.1 403 Forbidden');
    exit;
}

if($_SERVER['REQUEST_METHOD'] != 'POST'){
    header('HTTP/1.1 405 Method Not Allowed');
    header('Allow: POST');
    exit;
}

$avatar_file_array = $_FILES['avatar'] ?? null;
if(is_null($avatar_file_array)){
    header('HTTP/1.1 400 Bad Request');
    exit;
}

$state = ['success' => false];

// Commencer le traitement du fichier
// 1. Vérifier l'envoi correct
if($avatar_file_array['error'] != UPLOAD_ERR_OK){
    $state['cause'] = 'upload_nok';
    header('HTTP/1.1 422 Unprocessable Entity');
    send_json($state);
}

// 2. Vérifier la taille du fichier
if($avatar_file_array['size'] > MAXIMUM_FILE_SIZE){
    $state['cause'] = 'file_too_large';
    header('HTTP/1.1 422 Unprocessable Entity');
    send_json($state);
}

// 3. Vérifier le type de fichier avec le nom du fichier
$file_extension = strtolower(pathinfo($avatar_file_array['name'], PATHINFO_EXTENSION));

if(!in_array($file_extension, ALLOWED_EXTENSIONS)){
    $state['cause'] = 'denied_extension';
    $state['supported_extensions'] = ALLOWED_EXTENSIONS;

    header('HTTP/1.1 422 Unprocessable Entity');
    send_json($state);
}

// 4. Déplacer l'image.
$temporary_directory = create_temporary_directory('upload-');
$temporary_file_path = "$temporary_directory/picture.$file_extension";

if(!@move_uploaded_file($avatar_file_array['tmp_name'], $temporary_file_path)){
    header('HTTP/1.1 500 Internal Server Error');
    send_json($state);
}

// 5. Redimensionner l'image et la mettre dans le dossier définitif
$image = null;

// Repris de https://gist.github.com/janzikan/2994977
list($original_width, $original_height) = getimagesize($temporary_file_path);

// Calculer le "taux" de réduction selon ce qui est le plus court
$width_ratio = MAXIMUM_IMAGE_WIDTH / $original_width;
$height_ratio = MAXIMUM_IMAGE_HEIGHT / $original_height;
$ratio = min($width_ratio, $height_ratio);

$new_width = (int) $original_width * $ratio;
$new_height = (int) $original_height * $ratio;

// Créer une image que l'on pourra modifier avec image*
switch ($file_extension) {
    case 'png':
        $image = imagecreatefrompng($temporary_file_path);
        break;

    case 'jpg':
    case 'jpeg':
        $image = imagecreatefromjpeg($temporary_file_path);
        break;

    default:
        header('HTTP/1.1 500 Internal Server Error');
        send_json($state);
        exit;
}

// \!/ bien utiliser imagecreatetruecolor afin d'avoir une image avec plus de 256 couleurs.
$resized_image = imagecreatetruecolor($new_width, $new_height);
imagecopyresampled($resized_image, $image, 0, 0, 0, 0, $new_width, $new_height, $original_width, $original_height);

$users_repository = new UsersRepository(Database::getInstance());
$current_user = Auth::getUser($users_repository);
$new_filename = $current_user->id . '-' . sha1(uniqid()) . '.' . $file_extension;
$new_file_path = ROOT . "/public/user-content/$new_filename";

if(!imagepng($resized_image, $new_file_path)){
    header('HTTP/1.1 500 Internal Server Error');
    send_json($state);
}

imagedestroy($image);
imagedestroy($resized_image);

$current_user->avatar = $new_filename;
$users_repository->updateUser($current_user);

// Par défaut, retourne 200 OK
$state['success'] = true;
$state['filename'] = $new_filename;
send_json($state);