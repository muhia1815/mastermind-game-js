<?php

use App\Database;
use App\Repository\UsersRepository;
use App\Utilities\Auth;

require_once '../../includes/init.php';

if($_SERVER['REQUEST_METHOD'] != 'POST'){
    header('405 Method not allowed');
    header('Allow: POST');
    exit;
}

$repository = new UsersRepository(Database::getInstance());
Auth::requireLoggedInUser($repository);
$current_user = Auth::getUser($repository);

@unlink(ROOT . '/public/user-content/' . $current_user->avatar);

$current_user->avatar = null;
$repository->updateUser($current_user);
