<?php

use App\Database;
use App\Repository\UsersRepository;
use App\Utilities\Auth;

require_once '../../includes/init.php';

$repository = new UsersRepository(Database::getInstance());
Auth::requireLoggedInUser($repository);

$user = Auth::getUser($repository);

$stylesheets = ['/assets/css/profile-dropzone.css'];
$scripts = [
    ['file' => '/assets/js/profile-dropzone.js', 'type' => 'module'],
    ['file' => '/assets/js/avatar-deletion.js', 'type' => 'module']
];

$title = 'Mon compte';
require_once ROOT . '/includes/header.php';
/*
?>

<h2>Changer le mot de passe</h2>

<p>...</p>
*/
?>

<h2>Changer l'image de profil</h2>

<div id="profile-image-container" class="mb-2 <?= $user->avatar == null ? 'd-none' : '' ?>">
    <h3>Image de profil</h3>
    <img src="<?= '/user-content/' . htmlspecialchars($user->avatar) ?>" alt="Image de profil de <?= htmlspecialchars($user->username) ?>">

    <p>
        <button class="btn btn-error" id="delete-avatar-button">Supprimer l'image</button>
    </p>
</div>


<p class="toast d-none mb-2 mt-2" id="toast-container">
    <button class="btn btn-clear float-right js-toast-close"></button>
</p>

<div id="avatar_dropzone" class="file-dropzone">
    <p>Glissez-déposez votre nouvel avatar</p>
</div>

<?php
require_once ROOT . '/includes/footer.php';
?>
