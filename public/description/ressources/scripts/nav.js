const hamburger_button = document.querySelector(".hamburger");
const list_item_container = document.querySelector('header nav ul');

hamburger_button.addEventListener('click', (e) => {
    e.preventDefault();

    if(list_item_container.classList.contains("visible")){
        list_item_container.classList.remove("visible");
    } else {
        list_item_container.classList.add("visible");
    }
});