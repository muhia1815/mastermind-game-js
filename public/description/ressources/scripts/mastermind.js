//Canvas
const canvas = document.getElementById("canvas");
canvas.width = 400;
canvas.height = 805;
var ctx = canvas.getContext("2d");


//background image
/*
var bgImage = new Image();
bgImage.src = "ressources/images/mastermind.jpg";
bgImage.onload = function () {

  // ctx.drawImage(bgImage, 0, 0); 
}; */



        var colors = ['green', 'red', 'blue', 'white', 'yellow', 'plum'];

        var colorAdd = "#cad4d9";
        var numRows = 4;
        var numCols = 8;
        var radius = 25;
        var radiusPegs = 13.5;
        var color = "#cad4d9";
        var red = "#fc0303";
        var colorPegs = "#85868f";
        var pegsBlack = "#000000";
        var pegsWhite = "#ffffff";
        var isNextLigne = false;
        var pionsNoir = 0;
        var pionsBlanc = 0;
        var tabPionsNoir = [false, false, false, false] ;
        var tabPionsBlanc = [false, false, false, false] ;

        var currentLigne = 0;
        var currentCol = 7;
        var ligne = 0;
        var col = 7;

        var i = 0;
        var j = 14;
        var x = 0;
        var y = 0;
        var hasWin = false;

        //create the random color code
        var code = [
            colors[Math.floor(Math.random()*6)], 
            colors[Math.floor(Math.random()*6)],
            colors[Math.floor(Math.random()*6)],
            colors[Math.floor(Math.random()*6)]
        ];

        console.log(this.code);

        //dictionary of colors
        var colorsTab = {
            "rgb(0, 128, 0)": "green",
            "rgb(255, 255, 0)": "yellow",
            "rgb(255, 0, 0)": "red",
            "rgb(0, 0, 255)": "blue",
            "rgb(255, 192, 203)": "pink",
            "rgb(255, 165, 0)": "orange"
        }

        document.getElementById("ok").disabled = true ;


        // Set the colors you want to support in this array

        var proposition = [];
        var colorG = "#888888";

  



  function checkPegs(){
    document.getElementById("ok").disabled = true ;
    document.getElementById("clear").disabled = true ;

    //check si j'ai pions noir
    for (let index = 0; index < 4; index++) {
        if(this.proposition[index] === this.code[index]){
            this.pionsNoir ++;
            this.tabPionsNoir[index] = true;
        }
    }
    //check si j'ai pions blanc
    for (let index = 0; index < 4; index++) {
        if(this.tabPionsNoir[index] === true){
            continue;
        }
        for (let index2 = 0; index2 < 4; index2++) {
            if(this.tabPionsNoir[index2] === true){
                continue;
            }
            if(index === index2){
                continue;
            }
            if(this.proposition[index] === this.code[index2] && !this.tabPionsBlanc[index2]){
                this.tabPionsBlanc[index2] = true; 
                this.pionsBlanc ++ ;
                break;
            }
        }
    // this.buttonClear.disabled = false;
    }
    document.getElementById("clear").disabled = false ;
    
    drawPions();
      console.log("Pions noir : "+pionsNoir);
      console.log("Pions Blanc : "+pionsBlanc);

      if(this.pionsNoir != 4){
          for (let index = 1; index < 7; index++) {
           //   this.buttons[index].disabled = false;
            document.getElementById("b"+index).disabled = false;
          }
      } 

      this.pionsBlanc = 0;
      this.pionsNoir = 0 ;
      this.tabPionsNoir = [false, false, false, false] ;
      this.tabPionsBlanc = [false, false, false, false] ;

      this.col --;

      if(this.col < 0){
          //essayer d'afficher le msg aprés 3sec
          alert("Vous avez perdu mon ami !");
          document.getElementById("clear").disabled = true ;
      }
      this.isNextLigne = true;
  }


  function addColor(element){


    var colorAdd = element.style.backgroundColor;

    if(this.col < 0 ){
      for (let index = 1; index < 7; index++) {
          document.getElementById("b"+index).disabled = true;

      }
    }else{

      if(this.isNextLigne){
      ligne = 0;
      this.isNextLigne = false;
      }


      console.log(this.col);

      ajoutCouleur(colorAdd, ligne, col);
      proposition[ligne] = colorAdd ;

      if(ligne === 3){
          for (let index = 1; index < 7; index++) {
              this.document.getElementById("b"+index).disabled = true;
              this.document.getElementById("ok").disabled = false ;
          }
      }
      ligne ++ ;
    }

  }

  function initPegs(){

    for(let i = 0; i < 2; i++){
        for(let j = 0; j < numCols*2; j++){

        let x = calculXPegs(i);
        let y = calculYPegs(j);

        this.drawDot(x, y, radiusPegs, colorPegs )
        }
    }

  }

function initBoard(){
    for(var i = 0; i < numRows; i++) {
        for(var  j = 0; j < numCols; j++) {
            var x = calculX(i); 
            var y = calculY(j);
            drawDot(x, y, radius, color);
        }
    }      
}

function calculXPegs(i){
    return i*36+300;
}

function calculYPegs(j){
    return j*37+64;
}

function calculX(i){
    return i*65+35;
}

function calculY(j){
    return j * 75 + 75;
}


function drawDot(x, y, radius, color) {
    ctx.beginPath();
    ctx.arc(x, y, radius, 0, 2 * Math.PI, false);
    ctx.fillStyle = color;
    ctx.fill();
}  




function clearButton(){
    for (let index = 1; index < 7; index++) {
        document.getElementById("b"+index).disabled = false;
    }
    currentLigne = ligne -1 ;
    currentCol = col;
    console.log(currentLigne);
    console.log(currentCol);
    console.log(color);
    ajoutCouleur(color, currentLigne, currentCol);

    ligne --;

}

function ajoutCouleur(colors, ligne, col){
    let u = calculX(ligne);
    let v = calculY(col);
    drawDot(u, v, radius, colors);
}

function drawPions(){
    switch (pionsNoir) {
        case 0:
            switch (pionsBlanc) {
                case 1:
                    this.x = calculXPegs(i);
                    this.y = calculYPegs(j);
        
                    drawDot(x, y, radiusPegs, pegsWhite);
                    break;
                case 2:
                    for (let index = 0; index < pionsBlanc ; index++) {
                        x = calculXPegs(index);
                        y = calculYPegs(j);
        
                        drawDot(x, y, radiusPegs, pegsWhite);
                    }
                    break;
                case 3:
                    for (let index = 0; index < 2 ; index++) {
                        x = calculXPegs(index);
                        y = calculYPegs(j);
                
                        drawDot(x, y, radiusPegs, pegsWhite);
                    }
                    x = calculXPegs(i);
                    y = calculYPegs(j+1);
                
                    drawDot(x, y, radiusPegs, pegsWhite);
                    break;
                case 4:
                    for (let index = 0; index < 2 ; index++) {
                        for (let index2 = j; index2 < j+2; index2++) {
                            x = calculXPegs(index);
                            y = calculYPegs(index2);

                        drawDot(x, y, radiusPegs, pegsWhite);
                        }
                    }
                    break;    
                default:
                    break;
            }
        break;
            case 1:
                x = calculXPegs(i);
                y = calculYPegs(j);

                drawDot(x, y, radiusPegs, pegsBlack);
                switch (pionsBlanc) {
                    case 1:
                        x = calculXPegs(i+1);
                        y = calculYPegs(j);

                        drawDot(x, y, radiusPegs, pegsWhite);
                        break;
                    case 2:
                        x = calculXPegs(i+1);
                        y = calculYPegs(j);

                        drawDot(this.x, this.y, this.radiusPegs, this.pegsWhite);

                        this.x = calculXPegs(this.i);
                        this.y = calculYPegs(this.j+1);

                        drawDot(x, y, radiusPegs, pegsWhite);
                        break;
                    case 3:
                        x = calculXPegs(i+1);
                        y = calculYPegs(j);

                        drawDot(x, y, radiusPegs, pegsWhite);
                        for (let index = 0; index < pionsBlanc-1 ; index++) {
                            x = calculXPegs(index);
                            y = calculYPegs(j+1);
                
                            drawDot(x, y, radiusPegs, pegsWhite);
                            }
                        break;   
                    default:
                        break;
                }
                break;
        case 2:
            for (let index = 0; index < this.pionsNoir; index++) {
                x = calculXPegs(index);
                y = calculYPegs(j);
                drawDot(x, y, radiusPegs, pegsBlack);
                }
            switch (pionsBlanc) {
                case 1:
                    x = calculXPegs(i);
                    y = calculYPegs(j+1);

                    drawDot(x, y, radiusPegs, pegsWhite);
                    break;
                case 2:
                for (let index = 0; index < this.pionsBlanc ; index++) {
                    x = calculXPegs(index);
                    y = calculYPegs(j+1);

                    drawDot(x, y, radiusPegs, pegsWhite);
                }
                    break;   
                default:
                    break;
            }
            break;
        case 3:
            for (let index = 0; index < 2 ; index++) {
                x = calculXPegs(index);
                y = calculYPegs(j);
                console.log(index);
                drawDot(x, y, radiusPegs, pegsBlack);
            }
            x = calculXPegs(i);
            y = calculYPegs(j+1);

            drawDot(x, y, radiusPegs, pegsBlack);
            break;
        case 4:
            for (let index = 0; index < 2 ; index++) {
                for (let index2 = j; index2 < j+2; index2++) {
                    x = calculXPegs(index);
                    y = calculYPegs(index2);

                    console.log(index+ " : "+ index2);
                    drawDot(x, y, radiusPegs, pegsBlack);
                }
            }
            hasWin = true;
            //essayer d'afficher le msg aprés 3sec
            alert("Congratulations, you have won!\nThe code will now be displayed.");

            document.getElementById("ok").disabled = true ;
            document.getElementById("clear").disabled = true ;

            //relancer le jeu 
            break;
        default:
            break;
    }

    //change de ligne 
    j = j-2;
}

var main = function(){

  initBoard();
  initPegs();
 

}

main();

 


