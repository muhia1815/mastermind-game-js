<?php

require_once('../includes/init.php');


$title = "Jouer";
$stylesheets = ['assets/css/style_mastermind.css'];
$scripts = [['file' => 'assets/js/game/local_game.js', 'type' => 'module']];

require_once ROOT . "/includes/header.php";
?>
    <p>
        <button class="btn" id="music" data-state="paused" disabled>Mettre de la musique</button>

        <button id="volume-increment" class="btn">+</button>
        <span id="volume">??%</span>
        <button id="volume-decrement" class="btn">-</button>
    </p>

    <canvas id="canvas">
    </canvas>

    <div id="colored-buttons-container" class="buttons-container mb-2">

    </div>

    <div id="user-interaction-buttons">
    </div>

    <div id="winning-combination" class="mt-2" style="display: none;">
        <p>Combinaison gagnante</p>
        <div id="combination-squares">

        </div>
    </div>

<?php
require_once ROOT . "/includes/footer.php";