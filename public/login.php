<?php

use App\Database;
use App\Repository\UsersRepository;
use App\Utilities\Form\Field;
use App\Utilities\SessionUtils;

require_once '../includes/init.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    $username = $_POST['username'] ?? null;
    $password = $_POST['password'] ?? null;

    $errors = [];

    if(is_null($username) || is_null($password)){
        $errors['username'][] = "Le nom d'utilisateur est obligatoire";
        $errors['password'][] = "Le mot de passe est obligatoire";

        SessionUtils::flash('error', 'Une erreur s\'est produite lors de la vérification des identifiants. Merci de vérifier les champs');
        SessionUtils::flash('errors', $errors);
        SessionUtils::flash('old_post', $_POST);

        header('Location: /login.php');
        die;
    }

    $users_repo = new UsersRepository(Database::getInstance());
    $user = $users_repo->getUserByUsername($username);

    if(is_null($user) || !password_verify($password, $user->password)){
        $errors['username'][] = "Nom d'utilisateur ou mot de passe incorrect";
        $errors['password'][] = "Nom d'utilisateur ou mot de passe incorrect";

        SessionUtils::flash('errors', $errors);
        header('Location: /login.php');
        die;
    }

    SessionUtils::flash('success', 'Connecté');
    $_SESSION['user_id'] = $user->id;
    session_regenerate_id();
    header('Location: /');
    die;
}

$errors = SessionUtils::flashGet('errors', []);
$title = "Se connecter";
require_once ROOT . "/includes/header.php";
?>

    <div class="column col-sm-12 col-md-8 col-4 col-mx-auto">
        <form action="/login.php" method="post">
            <?=
            (new Field('text', 'username', "Nom d'utilisateur"))
                ->setErrors($errors['username'] ?? [])
                ->setValue($old_values['username'] ?? null)
                ->pushAttributes(['required' => 1])
            ?>

            <?=
            (new Field('password', 'password', "Mot de passe"))
                ->setErrors($errors['password'] ?? [])
                ->setValue($old_values['password'] ?? null)
                ->pushAttributes(['required' => 1])
            ?>

            <input type="submit" value="Se connecter" class="btn btn-primary">
        </form>
    </div>
<?php
require_once ROOT . "/includes/footer.php";

