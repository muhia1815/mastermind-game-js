<?php
require_once '../includes/init.php';


$title = "Le reste";
$scripts = [
    '/assets/js/geolocation.js',
    '/assets/js/counter.js'
];

require_once '../includes/header.php';
?>
    <p>Votre position: <span id="geolocation"></span></p>
    <button class="btn" id="geo-button">Obtenir votre géolocalisation</button>

    <hr>

    <button id="counter-increment" class="btn">+</button>
    <span id="counter">0</span>
    <button id="counter-decrement" class="btn">-</button>

<?php
require_once '../includes/footer.php';
