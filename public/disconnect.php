<?php

use App\Utilities\Auth;
use App\Utilities\SessionUtils;

require_once '../includes/init.php';

Auth::logout();
SessionUtils::flash('success', 'Déconnecté');
session_regenerate_id();

header('Location: /');
exit;