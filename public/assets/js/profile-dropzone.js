import Toast from "./modules/Toast.js";

function uploadAvatarFile(file){
    // Préparer la requête
    const xhr = new XMLHttpRequest();

    const form_data = new FormData();
    form_data.set('avatar', file);

    xhr.upload.addEventListener('progress', (e) => {
        console.log('progress', e);
    });

    xhr.addEventListener('load', (e) => {
        console.log('load', e);

        // Au cas où c'est Nginx qui répond avec 413 Request Entity Too Large
        if(xhr.status === 413 && xhr.getResponseHeader('Content-Type') === 'text/html'){
            toast_notification.setTypeError();
            toast_notification.setText("Erreur de l'envoi du fichier: le fichier est trop gros pour le serveur");
            toast_notification.show();
            return;
        }

        // Vérifier qu'on ait pas une erreur du serveur HTTP. Les codes 403 et 405 ne devraient pas se produire.
        switch (xhr.status) {
            case 200:
                toast_notification.setText("L'avatar a été envoyé et enregistré avec succès");
                toast_notification.setTypeSuccess();

                const server_response = JSON.parse(xhr.responseText);
                const image_container = profile_image_container.querySelector('img');
                image_container.setAttribute('src', `/user-content/${server_response.filename}`);
                profile_image_container.classList.remove('d-none');

                break;

            case 400: // Le fichier n'a pas pu être envoyé sur PHP
                toast_notification.setText("Une erreur est survenue lors de l'envoi de l'avatar");
                toast_notification.setTypeError();
                break;

            case 422: // Problème avec le fichier lui-même
                let response = JSON.parse(xhr.responseText);
                toast_notification.setTypeError();

                switch(response.cause) {
                    case 'upload_nok':
                        toast_notification.setText("Une erreur est survenue lors de l'envoi de l'avatar");
                        break;
                    case 'file_too_large':
                        toast_notification.setText("Le fichier est trop gros; maximum 5 MB");
                        break;
                    case 'denied_extension':
                        const extensions = response.supported_extensions.join(', ');
                        toast_notification.setText(`Le fichier doit avoir l'extension suivante: ${extensions}`);
                        break;
                    default:
                        toast_notification.setText(`Le serveur a refusé le fichier pour une raison inconnue. Merci de contacter l'administrateur`);
                }

                break;

            case 500: // Erreur serveur quelconque
                toast_notification.setTypeError();
                toast_notification.setText("Une erreur serveur est survenue lors du traitement du fichier");
                break;
        }

        toast_notification.show();
    })

    // Ouvrir la connexion et envoyer
    xhr.open('POST', '/account/receive_avatar.php');
    xhr.send(form_data);
}

const avatar_dropzone = document.querySelector('#avatar_dropzone');
const toast_notification = new Toast('#toast-container');
const profile_image_container = document.querySelector('#profile-image-container');

avatar_dropzone.addEventListener('dragenter', (e) => {
    e.preventDefault();
    avatar_dropzone.classList.add('dragenter');
});

avatar_dropzone.addEventListener('dragleave', (e) => {
    e.preventDefault();
    avatar_dropzone.classList.remove('dragenter');
});

avatar_dropzone.addEventListener('dragover', (e) => {
    e.preventDefault();
})

avatar_dropzone.addEventListener('drop', (e) => {
    e.preventDefault();
    avatar_dropzone.classList.remove('dragenter');

    console.log('Drop');
    console.log(e.dataTransfer);

    // Récupérer le fichier déposé
    let datatransfer = e.dataTransfer;

    if (datatransfer.files.length === 0){
        return;
    }

    // Et tout envoyer
    uploadAvatarFile(datatransfer.files[0]);
});