function refreshCounter(n){
    counter.textContent = n;
}

const counter = document.querySelector("#counter");
const increment = document.querySelector('#counter-increment');
const decrement = document.querySelector("#counter-decrement");

let current_counter = window.localStorage.getItem('counter') || 0;
refreshCounter(current_counter);

increment.addEventListener('click', e => {
    current_counter++;
    window.localStorage.setItem('counter', current_counter);
    refreshCounter(current_counter);
});

decrement.addEventListener('click', e => {
    current_counter--;
    window.localStorage.setItem('counter', current_counter);
    refreshCounter(current_counter);
});
