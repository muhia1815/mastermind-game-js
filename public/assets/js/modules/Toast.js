export default class Toast {
    _toastContainer;
    _closeButton;
    textMessageField;

    constructor(toastSelector){
        this._toastContainer = document.querySelector(toastSelector);

        if(this._toastContainer === null){
            throw new Error(`Toast container with selector ${toastSelector} does not exist`);
        }

        this._closeButton = this._toastContainer.querySelector(".js-toast-close");
        //this.textMessageField = this.toastContainer.querySelector('.js-toast-content');

        if(this._closeButton !== null){
            this._initializeButtonEvent();
        }
    }

    show(){
        this._toastContainer.classList.remove('d-none');
    }

    close(){
        this._toastContainer.classList.add('d-none');
    }

    setTypeError(){
        this._toastContainer.classList.remove('toast-success');
        this._toastContainer.classList.remove('toast-warning');
        this._toastContainer.classList.add('toast-error');
    }

    setTypeSuccess(){
        this._toastContainer.classList.add('toast-success');
        this._toastContainer.classList.remove('toast-warning');
        this._toastContainer.classList.remove('toast-error');
    }

    setText(content){
        let content_node = this._toastContainer.querySelector('.js-toast-content');
        if(content_node === null){
            content_node = document.createElement('span');
            content_node.classList.add('js-toast-content');
            this._toastContainer.appendChild(content_node);
        }

        content_node.textContent = content;
    }

    _initializeButtonEvent(){
        this._closeButton.addEventListener('click', e => {
            e.preventDefault();
            this.close();
        })
    }
}