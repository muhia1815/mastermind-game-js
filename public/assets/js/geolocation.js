
const location_text = document.querySelector('#geolocation');
const location_btn = document.querySelector('#geo-button');

location_btn.addEventListener('click', e => {
    navigator.geolocation.getCurrentPosition(
        (position) => {
            location_text.textContent = `${position.coords.latitude},${position.coords.longitude}`;
        },

        (error) => {
            location_text.textContent = `erreur: ${error.message}`;
        }
    )
});
