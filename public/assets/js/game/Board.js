const GUESS_PEG_RADIUS = 25;
const GUESS_PEG_INTERCOLUMN_MARGIN = 15; // px
const GUESS_PEG_INTERLINE_MARGIN = 20; //px

const VERIFICATION_PEG_RADIUS = 10;
const VERIFICATION_PEG_INTERCOLUMN_MARGIN = 5; // px;

const GAME_CANVAS_MARGIN = 10;

const BLANK_PEG_BACKGROUND_COLOR = 'whitesmoke'

export const VERIFICATION_PEG_COLORS = {
    color_present: '#b1b1b1',
    correctly_placed: '#000'
}

export default class Board {
    _drawingCanvas;
    _drawingCanvasContext;
    lines = [];
    gameRowsCount;

    background;
    backgroundIsLoaded = false;

    constructor(canvas_element, game_rows_count = 8){
        this.gameRowsCount = game_rows_count;
        this._drawingCanvas = document.querySelector(canvas_element);
        this._drawingCanvasContext = this._drawingCanvas.getContext('2d');

        this.background = new Image();
        this.background.onload = () => {
            this.backgroundIsLoaded = true;
            this.refreshBoard();
        }

        this.background.src = "/assets/img/mastermind.jpg";
    }

    initializeBoard(){
        // Initialiser la largeur et hauteur selon les coordonnées des points les plus loins
        let furthest_verification_peg_x =
            this._calculateCoordinatesForVerificationPeg(0, 4).x
            - VERIFICATION_PEG_INTERCOLUMN_MARGIN + GAME_CANVAS_MARGIN;

        let furthest_guess_peg_y =
            this._calculateCoordinatesForGuessPeg(this.gameRowsCount, 0).y
            - GUESS_PEG_INTERLINE_MARGIN + GAME_CANVAS_MARGIN;

        this._drawingCanvas.width = furthest_verification_peg_x;
        this._drawingCanvas.height = furthest_guess_peg_y;
        this.lines = [];
        this.refreshBoard();
    }

    refreshBoard(){
        if(!this.backgroundIsLoaded){
            return;
        }

        this._drawingCanvasContext.drawImage(this.background, 0, 0);

        let drawing_array = this._completeWorkingLines([...this.lines]);
        drawing_array.reverse();

        // Dessiner le jeu
        for(let i = 0; i < this.gameRowsCount; i++){
            let current_line = drawing_array[i] || this.getBlankLine();
            this.drawLine(i, current_line);
        }
    }

    drawLine(line_number, line_contents){
        for(let i = 0; i < line_contents.guess.length; i++){
            let guess_peg_coordinates = this._calculateCoordinatesForGuessPeg(line_number, i);
            let line_element = line_contents.guess[i] || BLANK_PEG_BACKGROUND_COLOR;

            this._drawDot(guess_peg_coordinates.x, guess_peg_coordinates.y, GUESS_PEG_RADIUS, line_element);
        }

        for(let i = 0; i < line_contents.verif.length; i++){
            let verif_peg_coordinates = this._calculateCoordinatesForVerificationPeg(line_number, i);
            let line_element = line_contents.verif[i] || BLANK_PEG_BACKGROUND_COLOR;

            this._drawDot(verif_peg_coordinates.x, verif_peg_coordinates.y, VERIFICATION_PEG_RADIUS, line_element);
        }
    }

    getBlankLine(){
        return {
            guess: [null, null, null, null],
            verif: [null, null, null, null]
        }
    }

    _calculateCoordinatesForGuessPeg(row_number, column_number){
        // Formule: ligne courante * (diamètre du cercle précédent + marge)
        //          + rayon du cercle pour qu'il ne soit pas coupé
        return {
            // Ligne
            y:
                row_number
                * (GUESS_PEG_RADIUS * 2 + GUESS_PEG_INTERLINE_MARGIN)
                + GUESS_PEG_RADIUS + GAME_CANVAS_MARGIN,

            // Colonne
            x:
                column_number
                * (GUESS_PEG_RADIUS * 2 + GUESS_PEG_INTERCOLUMN_MARGIN)
                + GUESS_PEG_RADIUS + GAME_CANVAS_MARGIN
        };
    }

    _calculateCoordinatesForVerificationPeg(row_number, column_number){
        let left_margin =
            this._calculateCoordinatesForGuessPeg(0, 4).x
            - GUESS_PEG_INTERCOLUMN_MARGIN
        ;
        return {
            // Ligne
            y:
                row_number
                * (GUESS_PEG_RADIUS * 2 + GUESS_PEG_INTERLINE_MARGIN)
                + GUESS_PEG_RADIUS + GAME_CANVAS_MARGIN,

            // Colonne
            x:
                column_number
                * (VERIFICATION_PEG_RADIUS * 2 + VERIFICATION_PEG_INTERCOLUMN_MARGIN)
                + VERIFICATION_PEG_RADIUS
                + left_margin + GAME_CANVAS_MARGIN
        };
    }

    _drawDot(x, y, radius, color){
        this._drawingCanvasContext.beginPath();
        // Angle is expressed in radian degrees, so 360° is 2*PI
        this._drawingCanvasContext.arc(x, y, radius, 0, 2 * Math.PI);
        this._drawingCanvasContext.fillStyle = color;
        this._drawingCanvasContext.fill();
    }

    _completeWorkingLines(workingLines){
        for (let i = workingLines.length; i < this.gameRowsCount; i++) {
            workingLines.push(this.getBlankLine());
        }

        return workingLines;
    }
}