import GameBase from "./GameBase.js";
import {VERIFICATION_PEG_COLORS} from "./Board.js";

export default class LocalGame extends GameBase {
    winningArrangement = ['red', 'blue', 'red', 'blue'];

    /***
     * @param {string} canvas_element
     * @param {string} buttons_container
     * @param user_interaction_container
     */
    constructor(canvas_element, buttons_container, user_interaction_container) {
        super(
            canvas_element,
            buttons_container,
            ['red', 'blue', 'green', 'rgb(153, 153, 153)', 'yellow', 'plum'],
            user_interaction_container
        );
    }

    onColorButtonClick(color) {
        console.log(`Clicked on ${color}`);
    }

    onSubmitButtonClick() {
        let verification = this._checkCombination(this.prompt);
        this.setVerificationAt(verification);

        if(verification.filter(peg => peg === VERIFICATION_PEG_COLORS.correctly_placed).length === 4){
            this._endGame(true);
            return;
        }

        this.passOnNextLine();
    }

    initialize() {
        for (let i = 0; i < 4; i++) {
            this.winningArrangement[i] = this.colors[Math.floor(Math.random() * this.colors.length)];
        }

        console.log(this.winningArrangement)

        super.initialize();
    }

    _endGame(won) {
        super._endGame(won);

        if(won){
            this.onGameWinning();
        } else {
            this.onGameLoosing();
        }
    }

    _checkCombination(prompt){
        let verification = new Array(4).fill(null);

        let pionsNoirs = 0;
        let pionsBlancs = 0;
        let pionsNoirsPresents = new Array(4).fill(false);
        let pionsBlancsPresents = new Array(4).fill(false);

        //check si j'ai pions noir
        for (let i = 0; i < 4; i++) {
            if (this.winningArrangement[i] === this.prompt[i]) {
                pionsNoirs++;
                pionsNoirsPresents[i] = true;
            }
        }

        //check si j'ai pions blanc
        for (let i = 0; i < 4; i++) {
            if (pionsNoirsPresents[i] === true) {
                continue;
            }

            for (let j = 0; j < 4; j++) {
                if (pionsNoirsPresents[j] === true || i === j) {
                    continue;
                }

                if (this.winningArrangement[i] === this.prompt[j] && !pionsBlancsPresents[j]) {
                    pionsBlancsPresents[j] = true;
                    pionsBlancs++
                    break;
                }
            }
        }


        verification.fill(VERIFICATION_PEG_COLORS.correctly_placed, 0, pionsNoirs);
        verification.fill(VERIFICATION_PEG_COLORS.color_present, pionsNoirs, pionsNoirs + pionsBlancs);
        console.log(verification, pionsNoirs, pionsBlancs, pionsNoirsPresents, pionsBlancsPresents);

        return verification;
    }

    // events utilisateur

    onGameWinning(){}

    onGameLoosing(){}
}
