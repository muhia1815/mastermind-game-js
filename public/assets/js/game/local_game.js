import LocalGame from "./LocalGame.js";

let game = new LocalGame('canvas', '#colored-buttons-container', '#user-interaction-buttons');
const musicButton = document.querySelector("#music");
const music = new Audio("/assets/music/gas-gas-gas.mp3");

const volume = document.querySelector("#volume");
const volume_increment = document.querySelector('#volume-increment');
const volume_decrement = document.querySelector("#volume-decrement");

let isPaused = true;

music.loop = true;
music.volume = 0.5;

game.onGameWinning = () => {
    alert('Et c\'est gagné !');
}

game.onGameLoosing = () => {
    alert('Perdu !');
}

game.onGameEnd = () => {
    let interaction_frame = document.querySelector('#user-interaction-buttons');
    let winning_combination_container = document.querySelector('#winning-combination');
    let winning_combination_tile_container = document.querySelector('#combination-squares');

    let elements = [];

    let button = document.createElement('button');
    button.classList.add('btn', 'mx-2');
    button.textContent = 'Rejouer';

    for(let winning_tile_color of game.winningArrangement){
        let tile = document.createElement('div');
        tile.classList.add('winning-square');
        tile.style.backgroundColor = winning_tile_color;
        winning_combination_tile_container.append(tile);
        elements.push(tile);
    }

    winning_combination_container.style.display = 'block';

    button.addEventListener('click', e => {
        e.preventDefault();

        winning_combination_container.style.display = 'none';

        button.remove();

        for (const element of elements) {
            element.remove();
        }

        game.initialize();
    });

    interaction_frame.appendChild(button);
}

game.initialize();


music.addEventListener('canplaythrough', e => {
    musicButton.removeAttribute('disabled');
});

musicButton.addEventListener('click', e => {
    if(isPaused){
        isPaused = false;
        music.play().then(() => console.log('On envoie le son !')).catch(e => console.error('Mais y\'a pu de son :('));
        musicButton.textContent = 'Mettre la musique sur pause';
    } else {
        isPaused = true;
        music.pause();
        musicButton.textContent = 'Mettre de la musique';
    }
})

let current_counter = music.volume;
volume.textContent = current_counter * 100 + "%";

volume_increment.addEventListener('click', e => {
    music.volume += .05;
    volume.textContent = Math.round(music.volume * 100) + "%";
});

volume_decrement.addEventListener('click', e => {
    music.volume -= .05;
    volume.textContent = Math.round(music.volume * 100) + "%";
});
