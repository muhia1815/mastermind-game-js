import Board from "./Board.js";

export default class GameBase {
    board;
    userInteractionContainer;
    currentRow = 0;
    colors;

    prompt = new Array(4).fill(null);

    colorButtonsContainer;

    _submitButton;
    _eraseButton;

    /***
     * @param {string} canvas_element
     * @param {string} buttons_container
     * @param {string[]} colors
     * @param {string} user_interaction_container
     */
    constructor(
        canvas_element,
        buttons_container,
        colors,
        user_interaction_container
    ){
        this.colors = colors;
        this.gameRowsCount = 8;
        this.colorButtonsContainer = document.querySelector(buttons_container);
        this.userInteractionContainer = document.querySelector(user_interaction_container);

        this.board = new Board(canvas_element, this.gameRowsCount);
    }

    /**
     * @param {int[]} guess_line
     * @param {int} row
     */
    setGuessLineAt(guess_line, row = this.currentRow){
        if(this.board.lines[row] === undefined){
            this.board.lines[row] = this.board.getBlankLine();
        }

        this.board.lines[row].guess = guess_line;
        this.board.refreshBoard();
    }

    setVerificationAt(verification, row = this.currentRow){
        if(this.board.lines[row] === undefined){
            this.board.lines[row] = this.board.getBlankLine();
        }

        this.board.lines[row].verif = verification;
        this.board.refreshBoard();
    }

    passOnNextLine(){
        this.currentRow++;

        // Si c'est strictement égal après incrémentation, on commence à bien dépasser le buffer. On envoie le signal
        // de fin de jeu
        if(this.currentRow === this.gameRowsCount){
            this._endGame(false);
            return;
        }

        this.prompt = new Array(4).fill(null);
        this._handleActionButtonsState();

        let blank = this.board.getBlankLine();
        this.setGuessLineAt(blank.guess);
        this.setVerificationAt(blank.verif);
    }

    initialize(){
        this.board.initializeBoard();
        this.prompt = new Array(4).fill(null);
        this._createButtons();
        this._handleActionButtonsState();
        this.currentRow = 0;
    }

    _endGame(won){
        this.colorButtonsContainer.querySelectorAll('button').forEach(e => { e.remove() })
        this._submitButton.remove();
        this._eraseButton.remove();
        this.onGameEnd();
    }

    _createButtons(){
        for(let color of this.colors){
            let button = document.createElement('button');
            button.classList.add('color-button');
            button.style.backgroundColor = color;
            this.colorButtonsContainer.appendChild(button);

            button.addEventListener('click', (e) => {
                e.preventDefault();
                let clicked_color = e.target.style.backgroundColor;

                this._addColorToPrompt(clicked_color)
                this.onColorButtonClick(clicked_color);
                this._handleActionButtonsState();
            });
        }

        this._submitButton = document.createElement('button');
        this._submitButton.textContent = 'Envoyer';
        this._submitButton.classList.add('btn', 'mx-2');
        this._submitButton.addEventListener('click', (e) => {
            e.preventDefault();
            this.onSubmitButtonClick();
        })
        this.userInteractionContainer.appendChild(this._submitButton);

        this._eraseButton = document.createElement('button');
        this._eraseButton.textContent = 'Effacer';
        this._eraseButton.classList.add('btn', 'mx-2');
        this._eraseButton.addEventListener('click', (e) => {
            e.preventDefault();
            this._removeLastColorFromPrompt();
            this._handleActionButtonsState();
        })

        this.userInteractionContainer.appendChild(this._eraseButton);
    }

    _addColorToPrompt(color){
        let nextNull = this.prompt.indexOf(null);
        if(nextNull === -1) return;

        this.prompt[nextNull] = color;
        this.setGuessLineAt(this.prompt);
    }

    _removeLastColorFromPrompt(){
        let lastNull = this.prompt.indexOf(null);

        if(lastNull === -1){
            this.prompt[this.prompt.length - 1] = null;
        } else {
            this.prompt[lastNull - 1] = null;
        }

        this.setGuessLineAt(this.prompt);
    }

    _handleActionButtonsState(){
        if(this.prompt.indexOf(null) === -1){
            this.colorButtonsContainer.querySelectorAll('button').forEach(e => { e.disabled = true; })
            this._submitButton.disabled = false;
        } else {
            this.colorButtonsContainer.querySelectorAll('button').forEach(e => { e.disabled = false; })
            this._submitButton.disabled = true;
        }
    }

    // Évents utilisateur
    onColorButtonClick(color){}

    onSubmitButtonClick(){}

    onGameEnd(){}
}