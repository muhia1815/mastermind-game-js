import Toast from "./modules/Toast.js";

async function deleteAvatar(){
    const response = await fetch('/account/delete_avatar.php', {
        method: 'POST',
        headers: {
            "Accept": "application/json"
        }
    });

    return response.ok;
}

const delete_avatar_button = document.querySelector('#delete-avatar-button');
const deletion_notification = new Toast("#toast-container");
const profile_image_container = document.querySelector("#profile-image-container");

delete_avatar_button.addEventListener('click', e => {
    e.preventDefault();
    deleteAvatar()
        .then(success => {
            if (success) {
                deletion_notification.setText("L'avatar a bien été supprimé");
                deletion_notification.setTypeSuccess();
                profile_image_container.classList.add('d-none');
            } else {
                deletion_notification.setText("Une erreur serveur s'est produite lors de la suppression de l'avatar");
                deletion_notification.setTypeError();
            }

            deletion_notification.show();
        })
        .catch(e => {
            deletion_notification.setTypeError();
            deletion_notification.setText("Une erreur réseau s'est produite lors de la suppression de l'avatar");
            deletion_notification.show();
        })
});