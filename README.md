# RIA - Projet Mastermind

## Prérequis pour le déploiement en local

- Avoir VirtualBox
- Avoir Vagrant

## Déploiement local

Avec une ligne de commandes, se déplacer dans le dossier du projet. Ensuite, taper `vagrant up` pour télécharger les fichiers de base de la machine virtuelle et lancer les commandes nécessaires.

Une fois que la machine virtuelle est déployée, le jeu est accessible sur [localhost:8080](http://localhost:8080)